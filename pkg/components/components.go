package components

import gerrors "github.com/nononsensecode/go-base/errors"

const (
	Auth gerrors.Component = "auth"
)
