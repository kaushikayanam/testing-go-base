package application

import (
	"context"

	"gitlab.com/kaushikayanam/testing-go-base/pkg/domain/model/user"
)

type UserService struct {
	userRepo user.Repository
}

func NewUserService(r user.Repository) UserService {
	if r == nil {
		panic("user repository is nil")
	}

	return UserService{
		userRepo: r,
	}
}

func (us UserService) Register(ctx context.Context, username, firstName,
	lastName, email, password string) (registered user.User, err error) {
	registered, err = user.New(username, firstName, lastName, email, password)
	if err != nil {
		return
	}

	registered, err = us.userRepo.Save(ctx, registered)
	return
}
