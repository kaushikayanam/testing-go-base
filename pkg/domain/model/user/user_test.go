package user

import (
	"fmt"
	"testing"
)

func Test_Domain_User_New(t *testing.T) {
	username, firstName, lastName, email, password, newPassword, incorrect := "kaushik", "kaushik", "asokan", "kaushik@gmail.com", "password", "new-password", "incorrect"

	u, err := New(username, firstName, lastName, email, password)

	if err != nil {
		t.Errorf("error shoule not be occurred while creating new user: %v", err)
	}

	if u.ID() != 0 {
		t.Errorf("expected 0, got %d", u.ID())
	}

	if u.Username() != username {
		t.Errorf("expected username is %s, got %s", username, u.Username())
	}

	if u.FirstName() != firstName {
		t.Errorf("expected first name is %s, got %s", firstName, u.FirstName())
	}

	if u.LastName() != lastName {
		t.Errorf("expected last name is %s, got %s", lastName, u.LastName())
	}

	if u.Email() != email {
		t.Errorf("expected email is %s, got %s", email, u.Email())
	}

	if err := u.MatchPassword(password); err != nil {
		t.Errorf("password \"%s\" should be matched, but error occurred: %v", password, err)
	}

	if err := u.ChangePassword(newPassword); err != nil {
		t.Errorf("password \"%s\" should be set as new password, but error occurred: %v", newPassword, err)
	}

	if err := u.MatchPassword(newPassword); err != nil {
		t.Errorf("new password \"%s\" should be matched, but error occurred: %v", newPassword, err)
	}

	if err := u.MatchPassword(incorrect); err == nil {
		t.Errorf("incorrect password \"%s\" should have returned an error", incorrect)
	}
}

func Test_Domain_User_Hash(t *testing.T) {
	password := "password"
	_, err := hashPassword(password)
	if err != nil {
		t.Errorf("hashing password should not return an error: %v", err)
	}
}

func Test_Domain_User_Unamrshal(t *testing.T) {
	id, username, firstName, lastName, email, password, hashed := 1, "kaushik", "kaushik", "asokan", "kaushik@gmail.com", "password", "$2a$10$pjXElHYhbkiYfXyh4Uqw8eZlSxQprgtky2lqYMq1dL6JLsv5swgom"
	fullName := fmt.Sprintf("%s %s", firstName, lastName)

	u := UnmarshalFromPersistence(int64(id), username, firstName, lastName, email, hashed)

	if u.ID() != 1 {
		t.Errorf("expected 0, got %d", u.ID())
	}

	if u.Username() != username {
		t.Errorf("expected username is %s, got %s", username, u.Username())
	}

	if u.FirstName() != firstName {
		t.Errorf("expected first name is %s, got %s", firstName, u.FirstName())
	}

	if u.LastName() != lastName {
		t.Errorf("expected last name is %s, got %s", lastName, u.LastName())
	}

	if u.Email() != email {
		t.Errorf("expected email is %s, got %s", email, u.Email())
	}

	if u.FullName() != fullName {
		t.Errorf("expected full name is %s, but got %s", fullName, u.FullName())
	}

	if u.Password() != hashed {
		t.Errorf("hashed password should be %s, but got %s", hashed, u.Password())
	}

	if err := u.MatchPassword("password"); err != nil {
		t.Errorf("password \"%s\" should be matched, but error occurred: %v", password, err)
	}

}
