package user

type Address struct {
	street string
	state  string
	pin    string
}

func (a Address) Street() string {
	return a.street
}

func (a Address) State() string {
	return a.state
}

func (a Address) Pin() string {
	return a.pin
}
