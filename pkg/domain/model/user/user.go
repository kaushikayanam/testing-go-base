package user

import (
	"fmt"

	gerrors "github.com/nononsensecode/go-base/errors"
	"gitlab.com/kaushikayanam/testing-go-base/pkg/components"
	"golang.org/x/crypto/bcrypt"
)

const (
	UserPasswordHashError gerrors.Code = iota + 100
	UserPasswordMatchError
)

func New(username, firstName, lastName, email, password string) (u User, err error) {
	var hashed string
	hashed, err = hashPassword(password)
	if err != nil {
		err = gerrors.NewUnknownError(UserPasswordHashError, components.Auth, err)
		return
	}

	u = User{
		username:  username,
		firstName: firstName,
		lastName:  lastName,
		email:     email,
		password:  hashed,
	}
	return
}

func UnmarshalFromPersistence(id int64, username, firstName, lastName, email, password string) User {
	return User{
		id:        id,
		username:  username,
		firstName: firstName,
		lastName:  lastName,
		email:     email,
		password:  password,
	}
}

type User struct {
	id        int64
	username  string
	firstName string
	lastName  string
	email     string
	password  string
	address   Address
}

func (u User) ID() int64 {
	return u.id
}

func (u User) Username() string {
	return u.username
}

func (u User) FirstName() string {
	return u.firstName
}

func (u User) LastName() string {
	return u.lastName
}

func (u User) FullName() string {
	return fmt.Sprintf("%s %s", u.firstName, u.lastName)
}

func (u User) Email() string {
	return u.email
}

func (u User) Password() string {
	return u.password
}

func (u User) Address() Address {
	return u.address
}

func (u User) AddressAsString() string {
	return fmt.Sprintf("%s, %s, PIN: %s", u.address.street, u.address.state, u.address.pin)
}

func (u *User) ChangePassword(p string) (err error) {
	var hashed string
	hashed, err = hashPassword(p)
	if err != nil {
		err = gerrors.NewUnknownError(UserPasswordHashError, components.Auth, err)
		return
	}

	u.password = hashed
	return
}

func (u User) MatchPassword(p string) (err error) {
	err = bcrypt.CompareHashAndPassword([]byte(u.password), []byte(p))
	if err != nil {
		err = gerrors.NewAuthorizationError(UserPasswordMatchError, components.Auth, err)
		return
	}
	return
}

func hashPassword(p string) (hashed string, err error) {
	var hashedBytes []byte
	hashedBytes, err = bcrypt.GenerateFromPassword([]byte(p), bcrypt.DefaultCost)
	if err == nil {
		hashed = string(hashedBytes)
		return
	}
	return
}
