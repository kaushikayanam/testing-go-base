package openapi

import (
	"net/http"

	"github.com/go-chi/render"
	gerrors "github.com/nononsensecode/go-base/errors"
	"github.com/nononsensecode/go-base/interfaces/httpsrvr/httperr"
	log "github.com/sirupsen/logrus"
	"gitlab.com/kaushikayanam/testing-go-base/pkg/components"
)

const (
	UserRegistrationInputError gerrors.Code = iota + 300
	UserRegistrationRenderError
)

func (s Server) RegisterUser(w http.ResponseWriter, r *http.Request) {
	log.Info("Request came for registering a user")
	data := new(RegisterUserJSONRequestBody)
	if err := render.Bind(r, data); err != nil {
		err = gerrors.NewIncorrectInputError(UserRegistrationInputError, components.Auth, err)
		httperr.RespondWithApiError(err, w, r)
		return
	}

	log.Info("registration data retrieved from request successfully")

	registered, err := s.UserService.Register(r.Context(), data.Username,
		data.FirstName, data.LastName, string(data.Email), data.Password)
	if err != nil {
		httperr.RespondWithApiError(err, w, r)
		return
	}

	log.WithFields(log.Fields{
		"id": registered.ID(),
	}).Info("user registered successfully")

	userId := UserId{Id: int32(registered.ID())}
	render.Status(r, http.StatusCreated)
	if err = render.Render(w, r, userId); err != nil {
		err = gerrors.NewUnknownError(UserRegistrationRenderError, components.Auth, err)
		httperr.RespondWithApiError(err, w, r)
		return
	}
}

func (u *RegisterUserJSONRequestBody) Bind(r *http.Request) error {
	return nil
}

func (u UserId) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}
