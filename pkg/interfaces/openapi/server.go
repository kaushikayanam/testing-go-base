package openapi

import (
	"gitlab.com/kaushikayanam/testing-go-base/pkg/application"
	"gitlab.com/kaushikayanam/testing-go-base/pkg/domain/model/user"
)

type Server struct {
	UserService application.UserService
}

func NewServer(userRepo user.Repository) Server {
	if userRepo == nil {
		panic("user repository is nil")
	}

	return Server{
		UserService: application.NewUserService(userRepo),
	}
}
