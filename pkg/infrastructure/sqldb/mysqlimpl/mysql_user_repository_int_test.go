package mysqlimpl

import (
	"context"
	"database/sql"
	"os"
	"testing"

	_ "github.com/go-sql-driver/mysql"
	"github.com/golang-migrate/migrate"
	mysqlmigrate "github.com/golang-migrate/migrate/database/mysql"
	_ "github.com/golang-migrate/migrate/source/file"
	"github.com/stretchr/testify/require"
	"gitlab.com/kaushikayanam/testing-go-base/pkg/domain/model/user"
)

var (
	db *sql.DB
)

func TestMain(m *testing.M) {
	var err error
	connStr := os.Getenv("MYSQL_CONN_STR")
	db, err = sql.Open("mysql", connStr)
	if err != nil {
		panic(err)
	}

	os.Exit(m.Run())
}

func Test_Infra_Sql_UserRepo_Mysql_IT_Save(t *testing.T) {
	var err error
	connStr := os.Getenv("MYSQL_CONN_STR")
	db, err = sql.Open("mysql", connStr)
	require.Nil(t, err)
	driver, err := mysqlmigrate.WithInstance(db, &mysqlmigrate.Config{})
	require.Nil(t, err)
	m, err := migrate.NewWithDatabaseInstance("file://../../../../scripts/sql/mysql/migrations", "mysql", driver)
	require.Nil(t, err)

	if err := m.Up(); err != nil && err != migrate.ErrNoChange {
		require.NoErrorf(t, err, "migration up should not return errors: %v", err)
	}

	u, err := user.New("kaushik", "kaushik", "asokan", "kaushik@gmail.com", "password")
	require.Nil(t, err)

	repo := NewMySqlUserRepository(newMySqlProvider(db))
	_, err = repo.Save(context.Background(), u)
	require.Nil(t, err)
}

type mysqlProvider struct {
	db *sql.DB
}

func newMySqlProvider(db *sql.DB) (p mysqlProvider) {
	return mysqlProvider{
		db: db,
	}
}

func (m mysqlProvider) GetConnection(ctx context.Context) (*sql.DB, error) {
	return m.db, nil
}
