package mysqlimpl

import (
	"context"
	"database/sql"
	"errors"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	gerrors "github.com/nononsensecode/go-base/errors"
	"github.com/stretchr/testify/assert"
	"gitlab.com/kaushikayanam/testing-go-base/pkg/domain/model/user"
)

func Test_Infra_Sql_UserRepo_Mysql_Save(t *testing.T) {
	var (
		db   *sql.DB
		mock sqlmock.Sqlmock
		err  error
		repo MySqlUserRepository
		u    user.User
	)

	db, mock, err = sqlmock.New()
	if !assert.Nil(t, err, "creating mock db should not throw error") {
		return
	}
	defer db.Close()

	repo = NewMySqlUserRepository(newMysqlMockDbProvider(db))

	username, firstName, lastName, email, password := "kaushik", "kaushik", "asokan", "kaushik@gmail.com", "password"
	u, err = user.New(username, firstName, lastName, email, password)
	if !assert.Nil(t, err) {
		return
	}

	ctx := context.Background()

	mock.ExpectBegin()
	mock.ExpectPrepare("INSERT INTO users").
		ExpectExec().
		WithArgs(username, firstName, lastName, email, u.Password()).
		WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()

	u, err = repo.Save(ctx, u)
	if err != nil {
		t.Errorf("saving should not return any errors: %v", err)
		return
	}

	assert.Equal(t, int64(1), u.ID())

	err = mock.ExpectationsWereMet()
	assert.Nil(t, err, "there were unfulfilled expectations: %s", err)
}

func Test_Infra_Sql_UserRepo_Mysql_Save_Tx_CreationError(t *testing.T) {
	var (
		db   *sql.DB
		mock sqlmock.Sqlmock
		err  error
		repo MySqlUserRepository
		u    user.User
	)

	db, mock, err = sqlmock.New()
	if !assert.Nil(t, err, "creating mock db should not throw error") {
		return
	}
	defer db.Close()

	repo = NewMySqlUserRepository(newMysqlMockDbProvider(db))

	username, firstName, lastName, email, password := "kaushik", "kaushik", "asokan", "kaushik@gmail.com", "password"
	u, err = user.New(username, firstName, lastName, email, password)
	if !assert.Nil(t, err) {
		return
	}

	ctx := context.Background()

	mock.ExpectBegin().WillReturnError(errors.New("cannot create transaction"))
	_, err = repo.Save(ctx, u)
	if err == nil {
		t.Errorf("as transaction cannot be created, error should be returned")
		return
	}

	var (
		apiErr gerrors.ApiError
		ok     bool
	)
	if apiErr, ok = err.(gerrors.ApiError); !ok {
		t.Errorf("error should be of api error type, but got error")
		return
	}

	if apiErr.Code() != MySqlDbUserSaveTxCreationError {
		t.Errorf("expected code %d, but got %d", MySqlDbUserSaveTxCreationError, apiErr.Code())
	}
}

func Test_Infra_Sql_UserRepo_Mysql_Save_Tx_FinishError(t *testing.T) {
	var (
		db   *sql.DB
		mock sqlmock.Sqlmock
		err  error
		repo MySqlUserRepository
		u    user.User
	)

	db, mock, err = sqlmock.New()
	if !assert.Nil(t, err, "creating mock db should not throw error") {
		return
	}
	defer db.Close()

	repo = NewMySqlUserRepository(newMysqlMockDbProvider(db))

	username, firstName, lastName, email, password := "kaushik", "kaushik", "asokan", "kaushik@gmail.com", "password"
	u, err = user.New(username, firstName, lastName, email, password)
	if !assert.Nil(t, err) {
		return
	}

	ctx := context.Background()

	mock.ExpectBegin()
	mock.ExpectPrepare("INSERT INTO users").
		ExpectExec().
		WithArgs(username, firstName, lastName, email, u.Password()).
		WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit().WillReturnError(errors.New("cannot commit"))

	_, err = repo.Save(ctx, u)
	if err == nil {
		t.Errorf("saving should return error as tx commit was a failure")
		return
	}

	var (
		apiErr gerrors.ApiError
		ok     bool
	)
	if apiErr, ok = err.(gerrors.ApiError); !ok {
		t.Errorf("error should be of api error type, but got error")
		return
	}

	if apiErr.Code() != MySqlDbUserSaveTxFinishError {
		t.Errorf("expected code %d, but got %d", MySqlDbUserSaveTxFinishError, apiErr.Code())
	}

	err = mock.ExpectationsWereMet()
	assert.Nil(t, err, "there were unfulfilled expectations: %s", err)
}

type mysqlMockDbProvider struct {
	db *sql.DB
}

func newMysqlMockDbProvider(db *sql.DB) (provider mysqlMockDbProvider) {
	provider = mysqlMockDbProvider{db: db}
	return
}

func (m mysqlMockDbProvider) GetConnection(ctx context.Context) (db *sql.DB, err error) {
	return m.db, nil
}
