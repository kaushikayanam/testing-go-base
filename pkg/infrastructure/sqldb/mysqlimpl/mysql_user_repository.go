package mysqlimpl

import (
	"context"
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
	gerrors "github.com/nononsensecode/go-base/errors"
	"github.com/nononsensecode/go-base/infrastructure/sqldb"
	"gitlab.com/kaushikayanam/testing-go-base/pkg/components"
	"gitlab.com/kaushikayanam/testing-go-base/pkg/domain/model/user"
)

const (
	MySqlDbUserSaveTxCreationError gerrors.Code = iota + 200
	MySqlDbUserSaveTxFinishError
	MySqlDbUserSaveStmtPrepareError
	MySqlDbUserSaveStmtExecError
	MySqlDbUserSaveReturnIdError
	MySqlDbUserByEmailNotFoundError
	MySqlDbUserByEmailQueryError
	MySqlDbUserByEmailReadError
)

type MySqlUserRepository struct {
	connectionProvider sqldb.ConnectionProvider
}

func NewMySqlUserRepository(cp sqldb.ConnectionProvider) MySqlUserRepository {
	if cp == nil {
		panic("connection provider is nil")
	}

	return MySqlUserRepository{
		connectionProvider: cp,
	}
}

func (r MySqlUserRepository) Save(ctx context.Context, u user.User) (saved user.User, err error) {
	var db *sql.DB
	db, err = r.connectionProvider.GetConnection(ctx)
	if err != nil {
		return
	}

	tx, err := db.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		err = gerrors.NewUnknownError(MySqlDbUserSaveTxCreationError, components.Auth, err)
		return
	}
	defer func() {
		err = sqldb.FinishTransaction(tx, err)
		if err != nil {
			err = gerrors.NewUnknownError(MySqlDbUserSaveTxFinishError, components.Auth, err)
			return
		}
	}()

	stmt, err := tx.PrepareContext(ctx, "INSERT INTO users (username, first_name, last_name, email, password) VALUES (?, ?, ?, ?, ?)")
	if err != nil {
		err = gerrors.NewUnknownError(MySqlDbUserSaveStmtPrepareError, components.Auth, err)
		return
	}
	defer stmt.Close()

	result, err := stmt.ExecContext(ctx, u.Username(), u.FirstName(), u.LastName(), u.Email(), u.Password())
	if err != nil {
		err = gerrors.NewUnknownError(MySqlDbUserSaveStmtExecError, components.Auth, err)
		return
	}

	id, err := result.LastInsertId()
	if err != nil {
		err = gerrors.NewUnknownError(MySqlDbUserSaveReturnIdError, components.Auth, err)
		return
	}

	stmt, err = tx.PrepareContext(ctx, "INSERT INTO address (user_id, stree, state, pin) VALUES (?, ?, ?, ?)")
	if err != nil {
		err = gerrors.NewUnknownError(MySqlDbUserSaveStmtPrepareError, components.Auth, err)
		return
	}

	_, err = stmt.ExecContext(ctx, id, u.Address().Street(), u.Address().State(), u.Address().Pin())
	if err != nil {
		err = gerrors.NewUnknownError(MySqlDbUserSaveStmtExecError, components.Auth, err)
		return
	}

	saved = user.UnmarshalFromPersistence(id, u.Username(), u.FirstName(), u.LastName(), u.Email(), u.Password())
	return
}

func (r MySqlUserRepository) FindByEmail(ctx context.Context, emailId string) (u user.User, err error) {
	var db *sql.DB
	db, err = r.connectionProvider.GetConnection(ctx)
	if err != nil {
		return
	}

	row := db.QueryRow("SELECT id, user_name, first_name, last_name, email, password FROM user WHERE email = ?", emailId)
	if err = row.Err(); err != nil {
		switch err {
		case sql.ErrNoRows:
			err = gerrors.NewNotFoundError(MySqlDbUserByEmailNotFoundError, components.Auth, err)
			return
		default:
			err = gerrors.NewUnknownError(MySqlDbUserByEmailQueryError, components.Auth, err)
			return
		}
	}

	var (
		id        int64
		username  string
		firstName string
		lastName  string
		email     string
		password  string
	)
	if err = row.Scan(&id, &username, &firstName, &lastName, &email, &password); err != nil {
		err = gerrors.NewUnknownError(MySqlDbUserByEmailReadError, components.Auth, err)
		return
	}

	u = user.UnmarshalFromPersistence(id, username, firstName, lastName, email, password)
	return
}
