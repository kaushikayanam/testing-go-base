ENV_MYSQL_TEST=\
	MYSQL_CONN_STR="root:mypassword@tcp(localhost:3306)/auth_user"

.PHONY: openapi
openapi:
	oapi-codegen -generate chi-server -o pkg/interfaces/openapi/openapi_api_gen.go -package openapi api/auth.yml
	oapi-codegen -generate types -o pkg/interfaces/openapi/openapi_types_gen.go -package openapi api/auth.yml

.PHONY: test
test:
	go test -cover -v ./...

.PHONY: test-cover
test-cover:
	rm -rvf build/tests/coverage
	mkdir -pv build/tests/coverage
	go test -coverprofile=build/tests/coverage/coverage.out -v ./...
	go tool cover -html=build/tests/coverage/coverage.out

.PHONY: test-domain
# incase multiple domain models
test-domain:
	go test -cover -v gitlab.com/kaushikayanam/testing-go-base/pkg/domain/model/user

.PHONY: test-domain-user
test-domain-user:
	go test -failfast -v -run '^Test_Domain_User.*' gitlab.com/kaushikayanam/testing-go-base/pkg/domain/model/user

.PHONY: test-infra-sql-userrepo-mysql
test-infra-sql-userrepo-mysql:
	go test -failfast -v gitlab.com/kaushikayanam/testing-go-base/pkg/infrastructure/sqldb/mysqlimpl

.PHONY: test-infra-sql-userrepo-mysql-it
test-infra-sql-userrepo-mysql-it:
	docker-compose up -d --remove-orphans mysql;
	$(ENV_MYSQL_TEST) \
	go test -failfast -v -run '^Test_Infra_Sql_UserRepo_Mysql_IT.*' gitlab.com/kaushikayanam/testing-go-base/pkg/infrastructure/sqldb/mysqlimpl
	docker-compose down
